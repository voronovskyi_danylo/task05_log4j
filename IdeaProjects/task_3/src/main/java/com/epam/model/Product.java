package com.epam.model;

import com.epam.exception.NotFoundException;

import java.util.ArrayList;
import java.util.List;

public class Product {

    private String name;
    private double price;
    private int number;
    private List<Product> products;

    public Product(){

    }

    public Product(Product product) {
        products = new ArrayList<Product>();
        products.add(product);
    }

    public Product(String name, double price, int number) {
        this.name = name;
        this.price = price;
        this.number = number;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getNubmer() {
        return number;
    }

    public void setNubmer(int nubmer) {
        this.number = nubmer;
    }

    @Override
    public String toString() {
        return "Product{" +
                "name='" + name + '\'' +
                ", price=" + price +
                ", number=" + number +
                '}';
    }
}
