package com.epam.controller;


import com.epam.model.DishDetergent;
import com.epam.model.FloorDetergent;
import com.epam.model.FurnitureDetergent;

import java.util.List;

public interface Controller {

    List<DishDetergent> getDishDetergentList();

    List<DishDetergent> dishBucketOperation();

    List<FloorDetergent> getFloorDetergentList();

    List<FloorDetergent> floorBucketOperation();

    List<FurnitureDetergent> getFurnitureDetergentList();

    List<FurnitureDetergent> furnitureBucketOperation();

}
