package com.epam.model;

import java.util.ArrayList;
import java.util.List;

public class DishDetergent extends Detergent {

    private double price;

    private List<DishDetergent> dishDetergentList;

    public DishDetergent() {
        dishDetergentList = new ArrayList<DishDetergent>();
        dishDetergentList.add(new DishDetergent("Fairy", "Dish detergent", 31.45));
        dishDetergentList.add(new DishDetergent("Gala", "Dish detergent", 45.25));
        dishDetergentList.add(new DishDetergent("Scala", "Dish detergent", 28.50));
        dishDetergentList.add(new DishDetergent("Our line", "Dish detergent", 15.33));
    }


    public DishDetergent(String name, String type, double price) {
        super(name, type);
        this.price = price;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public List<DishDetergent> getDishDetergentList() {
        for (int i = 0; i < dishDetergentList.size(); i++) {
            System.out.println(dishDetergentList.get(i));
        }
        return dishDetergentList;
    }

    public List<DishDetergent> dishBucketOperation() {
        return dishDetergentList;
    }

    public void setDishDetergentList(List<DishDetergent> dishDetergentList) {
        this.dishDetergentList = dishDetergentList;
    }

    @Override
    public String toString() {
        return  super.toString() + price + "$ ||";
    }
}
