package com.epam.model;

import java.util.List;

public class BusinessLogic implements Model {

    private DishDetergent dishDetergent;

    private FloorDetergent floorDetergent;

    private FurnitureDetergent furnitureDetergent;


    public BusinessLogic() {
        dishDetergent = new DishDetergent();
        floorDetergent = new FloorDetergent();
        furnitureDetergent = new FurnitureDetergent();
    }

    public List<DishDetergent> getDishDetergentList() {
        return dishDetergent.getDishDetergentList();
    }

    public List<DishDetergent> dishBucketOperation() {
        return dishDetergent.dishBucketOperation();
    }

    public List<FloorDetergent> getFloorDetergentList() {
        return floorDetergent.getFloorDetergentList();
    }

    public List<FloorDetergent> floorBucketOperation() {
        return floorDetergent.floorBucketOperation();
    }

    public List<FurnitureDetergent> getFurnitureDetergentList() {
        return furnitureDetergent.getFurnitureDetergentList();
    }

    public List<FurnitureDetergent> furnitureBucketOperation() {
        return furnitureDetergent.furnitureBucketOperation();
    }

}
