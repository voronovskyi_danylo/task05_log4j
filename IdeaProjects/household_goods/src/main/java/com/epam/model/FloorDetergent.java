package com.epam.model;

import java.util.ArrayList;
import java.util.List;

public class FloorDetergent extends Detergent {

    private double price;

    private List<FloorDetergent> floorDetergentList;

    public FloorDetergent() {
        floorDetergentList = new ArrayList<FloorDetergent>();
        floorDetergentList.add(new FloorDetergent("Mr Muscle", "Floor detergent", 132.99));
        floorDetergentList.add(new FloorDetergent("Domestos", "Floor detergent", 119.55));
        floorDetergentList.add(new FloorDetergent("Mr Proper", "Floor detergent", 139.15));
    }

    public FloorDetergent(String name, String type, double price) {
        super(name, type);
        this.price = price;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public List<FloorDetergent> getFloorDetergentList() {
        for (int i = 0; i < floorDetergentList.size(); i++) {
            System.out.println(floorDetergentList.get(i));
        }
        return floorDetergentList;
    }

    public List<FloorDetergent> floorBucketOperation() {
        return floorDetergentList;
    }


    public void setFloorDetergentList(List<FloorDetergent> floorDetergentList) {
        this.floorDetergentList = floorDetergentList;
    }

    @Override
    public String toString() {
        return  super.toString() + price + "$ ||";
    }
}
