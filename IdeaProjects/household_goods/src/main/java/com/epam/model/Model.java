package com.epam.model;

import java.util.List;

public interface Model {

    List<DishDetergent> getDishDetergentList();
    List<DishDetergent> dishBucketOperation();

    List<FloorDetergent> getFloorDetergentList();
    List<FloorDetergent> floorBucketOperation();

    List<FurnitureDetergent> getFurnitureDetergentList();
    List<FurnitureDetergent> furnitureBucketOperation();

}
