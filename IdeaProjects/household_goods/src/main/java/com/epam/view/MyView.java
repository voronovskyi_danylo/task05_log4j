package com.epam.view;

import com.epam.controller.Controller;
import com.epam.controller.ControllerImpl;
import com.epam.model.Detergent;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;


public class MyView {

    private Controller controller;
    private Map<Integer, Detergent> bucketList;
    private List<Detergent> list = new ArrayList<>();
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;
    private static Scanner input = new Scanner(System.in);
    private static Logger logger = LogManager.getLogger(MyView.class);

    public MyView() {
        controller = new ControllerImpl();
        menu = new LinkedHashMap<>();

        menu.put("1", "  1 - Show all dish detergents");
        menu.put("2", "  2 - Show all floor detergents");
        menu.put("3", "  3 - Show all furniture detergents");
        menu.put("4", "  4 - Buy some detergents");
        menu.put("Q", "  Q - exit");

        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", this::pressButton1);
        methodsMenu.put("2", this::pressButton2);
        methodsMenu.put("3", this::pressButton3);
        methodsMenu.put("4", this::pressButton4);

    }


    private void pressButton1() {
        controller.getDishDetergentList();
    }

    private void pressButton2() {
        controller.getFloorDetergentList();
    }

    private void pressButton3() {
        controller.getFurnitureDetergentList();
    }

    private void pressButton4() {
        bucketList = new LinkedHashMap<>();

        System.out.println("Here are all available products :");

        bucketList.put(1, controller.dishBucketOperation().get(0));
        bucketList.put(2, controller.dishBucketOperation().get(1));
        bucketList.put(3, controller.dishBucketOperation().get(2));
        bucketList.put(4, controller.dishBucketOperation().get(3));
        bucketList.put(5, controller.floorBucketOperation().get(0));
        bucketList.put(6, controller.floorBucketOperation().get(1));
        bucketList.put(7, controller.floorBucketOperation().get(2));
        bucketList.put(8, controller.furnitureBucketOperation().get(0));
        bucketList.put(9, controller.furnitureBucketOperation().get(1));
        bucketList.put(10, controller.furnitureBucketOperation().get(2));

        for (Detergent detergent : bucketList.values()) {
            System.out.println(detergent);
        }

        logger.trace("HERE IS TRACE");
        System.out.println("If you want check your bucket print 11 ");

        Integer test;
        Integer view = 11;

        do {
            System.out.println("Print detergent number to add it to bucket : ");
            test = input.nextInt();
            try {
                if (bucketList.containsKey(test)) {
                    list.add(bucketList.get(test));
                } else if (test == view) {
                    for (int i = 0; i < list.size(); i++) {
                        System.out.println(list.get(i));
                    }
                }
            } catch (Exception e) {
            }
        } while (!test.equals("Q"));

    }

    private void outputMenu() {
        System.out.println("\nWELCOME TO MY SHOP:");
        for (String str : menu.values()) {
            System.out.println(str);
        }
    }

    public void show() {
        String keyMenu;
        do {
            outputMenu();
            System.out.println("Please, select menu point.");
            keyMenu = input.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).print();
            } catch (Exception e) {
            }
        } while (!keyMenu.equals("Q"));
    }

}
